"""Python setup.py for ut_releases_notes package"""
import io
import os
from setuptools import find_packages, setup
import re

def read(*paths, **kwargs):
    """Read the contents of a text file safely.
    >>> read("ut_release_notes", "VERSION")
    '0.1.0'
    >>> read("README.md")
    ...
    """
    content = ""
    with io.open(
            os.path.join(os.path.dirname(__file__), *paths),
            encoding=kwargs.get("encoding", "utf8"),
    ) as open_file:
        content = open_file.read().strip()
    return content


def get_version():
    with open(os.path.join(os.path.dirname(__file__), 'ut_release_notes/version.py')) as f:
        result = re.search(r'{}\s*=\s*[\'"]([^\'"]*)[\'"]'.format('__version__'), f.read())
    return result.group(1)


def read_requirements(path):
    return [
        line.strip()
        for line in read(path).split("\n")
        if not line.startswith(('"', "#", "-", "git+"))
    ]


setup(
    name="ut_release_notes",
    version=get_version(),
    description="Generate release notes for Ubuntu Touch from related GitLab projects",
    url="https://gitlab.com/ubports/development/tooling/ut_release_notes",
    long_description=read("README.md"),
    long_description_content_type="text/markdown",
    author="sunweaver",
    packages=find_packages(exclude=[]),
    install_requires=['python-gitlab>=3.0'],
    entry_points={
        "console_scripts": ["ut-release-notes = ut_release_notes.generate:main"]
    },
)
