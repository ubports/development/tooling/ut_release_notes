import datetime
import gitlab
import os
import sys

from gitlab_release_notes import generate_release_notes as gitlab_release_notes

from .version import __version__

def recursive_project_list (gl, group):
    project_list = []

    if hasattr(group, 'subgroups'):

        subgroups = group.subgroups.list(get_all=True)
        for subgroup in subgroups:

            real_subgroup = gl.groups.get(subgroup.id, lazy=True)
            project_list.extend(recursive_project_list(gl, real_subgroup))

    if hasattr(group, 'projects'):
        projects = group.projects.list(get_all=True)
        for p in projects:
            project_list.append((p.path, p.id))

    return project_list

def ut_release_notes(group_id, endstr = '  <br>', since=None, **config):
    """
    Generate Ubuntu Touch release notes from GitLab

    Parameters
    ----------
    group_id: int
        ID of a subgroup in GitLab
    config: dict
        url: Optional[str] = None,
        private_token: Optional[str] = None,
        oauth_token: Optional[str] = None,
        job_token: Optional[str] = None,
        ssl_verify: Union[bool, str] = True,
        http_username: Optional[str] = None,
        http_password: Optional[str] = None,
        timeout: Optional[float] = None,
        api_version: str = '4',
        session: Optional[requests.sessions.Session] = None,
        per_page: Optional[int] = None,
        pagination: Optional[str] = None,
        order_by: Optional[str] = None,
        user_agent: str = 'python-gitlab/3.1.0',
        retry_transient_errors: bool = False,
    """

    gl = gitlab.Gitlab(**config)
    group = gl.groups.get(group_id)

    log = ""

    project_list = recursive_project_list(gl, group)
    project_list.sort()

    for p in project_list:
        id = p[1]
        try:
            notes = gitlab_release_notes(p[1],
                                         endstr=endstr,
                                         since=since,
                                         quiet=True,
                                         **config,
                    )
            if notes:
                log += notes
                log += f"{endstr}"
        except ValueError:
            # Ignore git repos that have never seen a merge request...
            pass

    return log


def main():
    import argparse
    parser = argparse.ArgumentParser(os.path.basename(sys.argv[0]),
                                     description="Generate release notes for Ubuntu Touch using \
                                     the merge request titles of related GitLab repositories")

    # Required
    parser.add_argument("group_id", type=int)
    # Optional
    parser.add_argument("--url", default="https://gitlab.com", required=False)
    parser.add_argument("--private_token", type=str, required=False, default=None)
    parser.add_argument('--version', action='version', version=__version__)
    parser.add_argument('--html', action='store_true')
    parser.add_argument('--since', type=datetime.date.fromisoformat, required=False, default=None)

    args = parser.parse_args()

    if args.html:
        endstr = '  <br>'
    else:
        endstr = '\n'
    notes = ut_release_notes(args.group_id,
                             url=args.url,
                             endstr=endstr,
                             since=args.since,
                             private_token=args.private_token,
            )
    print(notes)

if __name__ == "__main__":
    main()
